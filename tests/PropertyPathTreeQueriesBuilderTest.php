<?php
/** @noinspection SqlNoDataSourceInspection */

/** @noinspection SqlResolve */

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\PropertyPathTreeModels\IdPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RootPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\ManyToOne;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqlRelationshipModels\OneToOne;
use ch\_4thewin\SqlSelectModels\Arguments\IntArgument;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqlSelectModels\Page;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_AND;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_EQ;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\IdCondition;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEmpty;
use function PHPUnit\Framework\assertEquals;

class PropertyPathTreeQueriesBuilderTest extends TestCase
{
    public function testPaginationOnNonRoot() {
        // Same tree as in function testPaginationOnNonRoot of class TypedQueryRootToPropertyPathTreeConversionTest
        // of package ch\_4thewin\SqppTypedQueryToPropertyPathTreeConversionTests.
        $tree = (new RootPropertyPathNode('Person', new Table('Person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode((new IdPropertyPathNode('id',
                new ColumnExpression(
                    new Table('Person', 'id', 'string'),
                    'id',
                    'string'
                )))
                ->setIsNeededForToMany(true)
            )
            ->addSubNode((new RelationshipPropertyPathNode('friends',
                (new ManyToMany(
                    new Table('Person', 'id','string'),
                    // TODO composite key ['fk1', 'fk2'] instead of just fk1
                    new Table('person_friends', 'fk1','string'),
                    'fk1', 'string',
                    'fk2','string',
                    new Table('Person', 'id', 'string', 'friends'),
                ))
                    ->setPage(new Page(5, 10))
            ))
                ->setIsPartOfRenderedBranch(true)
                ->addSubNode((new LeafPropertyPathNode(
                    'name',
                    new ColumnExpression(
                        new Table('Person', 'id', 'string', 'friends'),
                        'name',
                        'string'
                    )))
                    ->setIsPartOfRenderedBranch(true)
                )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $build = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `Person`.`id` FROM `Person`',
            $build->getQueryString()
        );

        assert(1,count($collectionQueryNode->getSubNodes()));
        assertEquals(
            'SELECT * FROM (SELECT `person_friends`.`fk1`,`friends`.`name`,row_number() OVER '.
            '(PARTITION BY `person_friends`.`fk1`) as __rowNumber FROM `person_friends` JOIN `Person` `friends` '.
            'ON `friends`.`id` = `person_friends`.`fk2`) `person_friends` WHERE (`person_friends`.`__rowNumber` > ? '.
            'AND `person_friends`.`__rowNumber` < ?)',
            $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build()->getQueryString()
        );
        self::assertEmpty($build->getArguments());
    }

    public function testPaginationOnRoot() {
        // Same tree as in function testPaginationOnRoot of class TypedQueryRootToPropertyPathTreeConversionTest
        // of package ch\_4thewin\SqppTypedQueryToPropertyPathTreeConversionTests.
        $tree = (new RootPropertyPathNode('Person', new Table('Person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode((new LeafPropertyPathNode('name',
                new ColumnExpression(
                    new Table('Person', 'id', 'string'),
                    'name', 'string'
                )))
                ->setIsPartOfRenderedBranch(true)
            )
            ->setPage((new Page(
                5,
                10
            )));
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $build = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `Person`.`name` FROM `Person` LIMIT 5 OFFSET 10',
            $build->getQueryString()
        );
        self::assertEmpty($build->getArguments());
    }

    public function testSingleColumn()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'name',
                    new ColumnExpression(new Table('person', 'id', 'string'), 'name', 'string')
                ))
                    ->setIsPartOfRenderedBranch(true)
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $build = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `person`.`name` FROM `person`',
            $build->getQueryString()
        );
        self::assertEmpty($build->getArguments());
    }

    public function testManyToOne()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'address',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'currentAddress', 'string',
                        new Table('address', 'id', 'string','a')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'street',
                            new ColumnExpression(
                                new Table('address', 'id', 'string','a'),
                                'street',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'number', new ColumnExpression(
                                new Table('address', 'id', 'string', 'a'),
                                'number',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `a`.`street`,`a`.`number` FROM `person` LEFT JOIN `address` `a` ON `a`.`id` = `person`.`currentAddress`',
            $baseQueryBuild->getQueryString()
        );
        self::assertEmpty($baseQueryBuild->getArguments());
    }

    public function testSearchManyToOne()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->setIsPartOfFilteredBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'address',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'currentAddress', 'string', 
                        new Table('address', 'id', 'string')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsPartOfFilteredBranch(true)
                    ->setFilterCondition(
                        new _AND(
                            new _EQ(
                                new ColumnExpression(
                                    new Table('address', 'id', 'string'),
                                    'street',
                                    'string'
                                ), new StringArgument('Sesame Street')
                            ),
                            new _EQ(
                                new ColumnExpression(
                                    new Table('address', 'id', 'string'),
                                    'number',
                                    'string'
                                ), new StringArgument('42')
                            )
                        )

                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'street', new ColumnExpression(
                                new Table('address', 'id', 'string'),
                                'street',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'number', new ColumnExpression(
                                new Table('address', 'id', 'string'),
                                'number',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `address`.`street`,`address`.`number` FROM `person` JOIN `address` '.
            'ON `address`.`id` = `person`.`currentAddress` AND (`address`.`street` = ? AND `address`.`number` = ?)',
            $baseQueryBuild->getQueryString()
        );
        self::assertEquals(
            [new StringArgument('Sesame Street'), new StringArgument('42')],
            $baseQueryBuild->getArguments()
        );
    }

    public function testOneToOne()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'skillSet',
                    new OneToOne(
                        new Table('skillSet', 'id', 'string'),
                        'personId', 'string',
                        new Table('person', 'id', 'string'),
                        true
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id',
                            new ColumnExpression(
                                new Table('skillSet', 'id', 'string'),
                                'id',
                                'string'
                            )
                        ))
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'modificationDate', new ColumnExpression(
                                new Table('skillSet', 'id', 'string'),
                                'modificationDate',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `skillSet`.`modificationDate` FROM `person` LEFT JOIN `skillSet` ON `person`.`id` = `skillSet`.`personId`',
            $baseQueryBuild->getQueryString()
        );
        self::assertEmpty($baseQueryBuild->getArguments());
    }

    public function testSearchExactPrimaryKeyOneToOne()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->setIsPartOfFilteredBranch(true)
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'name',
                    new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'skillSet',
                    new OneToOne(
                        new Table('person', 'id', 'string'),
                        'skillSetId','string',
                        new Table('skillSet', 'id', 'string'),
                        false
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsPartOfFilteredBranch(true)
                    ->setIsOnlyIdRestricted(true)
                    ->setFilterCondition(
                        new IdCondition(
                            new ColumnExpression(
                                new Table('person', 'id', 'string'),
                                'skillSetId',
                                'string'
                            ), [new IntArgument(1), new IntArgument(2), new IntArgument(3)]
                        )
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'skillSetName',
                            new ColumnExpression(
                                new Table('skillSet', 'id', 'string'),
                                'skillSetName',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string'),
                                'skillSetId',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)


                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        self::assertEmpty($collectionQueryNode->getCollectionQuerySubNodes());
        $baseQuery = $collectionQueryNode->getQuery();
        $baseQueryBuild = $baseQuery->build();
        assertEquals(
        // TODO JOIN Type for skillSet has been set to INNER because skillSet is marked as "part of restricted branch".
        //   It is important to note that this approach does not allow searches like "skillSet should be NULL".
        //   For that purpose, another approach with a "global search" using a wrapped sql select will be implemented.
            'SELECT `person`.`name`,`person`.`skillSetId`,`skillSet`.`skillSetName` FROM `person` '.
            'JOIN `skillSet` ON `skillSet`.`id` = `person`.`skillSetId` WHERE `person`.`skillSetId` IN (?,?,?)',
            $baseQueryBuild->getQueryString()
        );
        self::assertEquals(
            [new IntArgument(1), new IntArgument(2), new IntArgument(3)],
            $baseQueryBuild->getArguments()
        );
    }

    public function testOneToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'publishedBooks',
                    new OneToMany(
                        new Table('publishedBooks', 'id', 'string'),
                        'personId','string',
                        new Table('person', 'id', 'string')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'bookTitle', new ColumnExpression(
                                new Table('publishedBooks', 'id', 'string'),
                                'bookTitle',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $baseQueryBuilder = $collectionQueryNode->getQuery();
        $baseQueryBuild = $baseQueryBuilder->build();
        $collectionQuerySubNode = $collectionQueryNode->getCollectionQuerySubNode('publishedBooks');
        $collectionQueryBuild = $collectionQuerySubNode->getQuery()->build();
        assertEquals(
            'SELECT `publishedBooks`.`personId`,`publishedBooks`.`bookTitle` FROM `publishedBooks`',
            $collectionQueryBuild->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testSearchedOneToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->setIsPartOfFilteredBranch(true)
            ->setFilterCondition(
                new _EQ(
                    new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    ), new StringArgument('Fritz')
                )
            )
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'fullName', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)


            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'publishedBooks',
                    (new OneToMany(
                        new Table('publishedBooks', 'id', 'string'),
                        'personId', 'string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsPartOfFilteredBranch(true)
                    ->setFilterCondition(
                        new _EQ(
                            new ColumnExpression(
                                new Table('publishedBooks', 'id', 'string'),
                                'bookTitle',
                                'string'
                            ), new StringArgument('Alice in Wonderland')
                        )
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'bookTitle', new ColumnExpression(
                                new Table('publishedBooks', 'id', 'string'),
                                'bookTitle',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        assertEquals(
            'SELECT `publishedBooks`.`personId`,`publishedBooks`.`bookTitle` FROM `publishedBooks`',
            $collectionQueryNode->getCollectionQuerySubNode('publishedBooks')->getQuery()->build()->getQueryString()
        );
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT DISTINCT `person`.`id`,`person`.`name` FROM `person` JOIN `publishedBooks` ON '.
            '`person`.`id` = `publishedBooks`.`personId` AND `publishedBooks`.`bookTitle` = ? WHERE `person`.`name` = ?',
            $baseQueryBuild->getQueryString()
        );
        assertEquals(
            [new StringArgument('Alice in Wonderland'), new StringArgument('Fritz')],
            $baseQueryBuild->getArguments()
        );
    }

    public function testManyToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'fullName', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friends',
                    (new ManyToMany(
                        new Table('person', 'id', 'string'),
                        new Table('friends', 'id', 'string'),
                        'person1','string',
                        'person2','string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'name', new ColumnExpression(
                                new Table('person', 'id', 'string'),
                                'name',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild = $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build();
        assertEquals(
            'SELECT `friends`.`person1`,`person`.`name` FROM `friends` JOIN `person` ON `person`.`id` = `friends`.`person2`',
            $collectionQueryBuild->getQueryString()
        );
        // TODO what if the table that is already in the FROM clause has to be joined on the same query? -> name conflict!
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `person`.`id`,`person`.`name` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testExactPrimaryKeySearchManyToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfFilteredBranch(true)
            ->setIsPartOfRenderedBranch(true)
            ->setFilterCondition(
                new IdCondition(
                    new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    ), [new IntArgument(5)]
                )
            )
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'fullName', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friends',
                    (new ManyToMany(
                        new Table('person', 'id', 'string'),
                        new Table('friends', 'id', 'string'),
                        'person1','string',
                        'person2','string',
                        new Table('person', 'id', 'string', 'person2')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsPartOfFilteredBranch(true)
                    ->setFilterCondition(
                        new _AND(
                            new _EQ(
                                new ColumnExpression(
                                    new Table('person', 'id', 'string', 'person2'),
                                    'name',
                                    'string'
                                ), new StringArgument('Hans')
                            ),
                            new IdCondition(
                                new ColumnExpression(
                                    new Table('friends', 'id', 'string'),
                                    'person2',
                                    'string'
                                ), [new IntArgument(2), new IntArgument(3), new IntArgument(4)]
                            )
                        )
                    )
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('friends', 'id', 'string'),
                                'person2',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'name', new ColumnExpression(
                                new Table('person', 'id', 'string', 'person2'),
                                'name',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild = $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `friends`.`person1`,`friends`.`person2`,`person2`.`name` FROM `friends` '.
            'JOIN `person` `person2` ON `person2`.`id` = `friends`.`person2`',
            $collectionQueryBuild->getQueryString()
        );
        assertEmpty($collectionQueryBuild->getArguments());
        assertEquals(
            'SELECT DISTINCT `person`.`id`,`person`.`name` FROM `person` JOIN `friends` ON '.
            '`person`.`id` = `friends`.`person1` JOIN `person` `person2` ON `person2`.`id` = `friends`.`person2` '.
            'AND (`person2`.`name` = ? AND `friends`.`person2` IN (?,?,?)) WHERE `person`.`id` = ?',
            $baseQueryBuild->getQueryString()
        );
        assertEquals(
            [
                new StringArgument('Hans'),
                new IntArgument(2),
                new IntArgument(3),
                new IntArgument(4),
                new IntArgument('5')
            ],
            $baseQueryBuild->getArguments()
        );
    }

    public function testIdentifierColumn()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'fullName', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friends',
                    (new ManyToMany(
                        new Table('person', 'id', 'string'),
                        new Table('friends', 'id', 'string'),
                        'person1','string',
                        'person2','string',
                        new Table('person', 'id', 'string', 'person2')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsOnlyIdRendered(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('friends', 'id', 'string'),
                                'person2',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild = $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `friends`.`person1`,`friends`.`person2` FROM `friends`',
            $collectionQueryBuild->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id`,`person`.`name` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testOwningToInverseId()
    {
        $tree = (new RootPropertyPathNode('root', new Table('rootTable', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'toOne',
                    new OneToOne(
                        new Table('rootTable', 'id', 'string'),
                        'foreignKeyColumnName','string',
                        new Table('inverseTable', 'id', 'string'),
                        false
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setIsOnlyIdRendered(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('rootTable', 'id', 'string'),
                                'foreignKeyColumnName',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
            );

        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        assertEquals(
            'SELECT `rootTable`.`foreignKeyColumnName` FROM `rootTable`',
            $collectionQueryNode->getQuery()->build()->getQueryString()
        );
    }


    public function testInverseToOwningId()
    {
        $tree = (new RootPropertyPathNode('root', new Table('rootTable', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('rootTable', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'toOne',
                    (new OneToOne(
                        new Table('owningTable', 'id', 'string'),
                        'foreignKeyColumnName','string',
                        new Table('inverseTable', 'id', 'string'),
                        true
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('owningTable', 'id', 'string'),
                                'id',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'friends',
                            (new ManyToMany(
                                new Table('inverseTable', 'id', 'string'),
                                new Table('friends', 'id', 'string'),
                                'person1','string',
                                'person2','string',
                                new Table('person', 'id', 'string')
                            ))
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'name', new ColumnExpression(
                                        new Table('person', 'id', 'string'),
                                        'name',
                                        'string'
                                    )
                                ))
                                    ->setIsPartOfRenderedBranch(true)

                            )
                    )
            );


        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild = $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `friends`.`person1`,`person`.`name` FROM `friends` JOIN `person` ON `person`.`id` = `friends`.`person2`',
            $collectionQueryBuild->getQueryString()
        );
        assertEquals(
            'SELECT `rootTable`.`id`,`owningTable`.`id` FROM `rootTable` LEFT JOIN `owningTable` ON '.
            '`inverseTable`.`id` = `owningTable`.`foreignKeyColumnName`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testNestedManyToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friends',
                    (new ManyToMany(
                        new Table('person', 'id', 'string'),
                        new Table('friends', 'id', 'string'),
                        'person1','string',
                        'person2','string',
                        new Table('person', 'id', 'string','my_friends')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('friends', 'id', 'string'),
                                'person2',
                                'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                        ->setIsNeededForToMany(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'friends',
                            (new ManyToMany(
                                new Table('person', 'id', 'string'),
                                new Table('friends', 'id', 'string'),
                                'person1','string',
                                'person2','string',
                                new Table('person', 'id', 'string','friends_of_my_friends')
                            ))
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new RelationshipPropertyPathNode(
                                    'toOne',
                                    (new OneToOne(
                                        new Table('owningTable', 'id', 'string'),
                                        'foreignKeyColumnName','string',
                                        new Table('inverseTable', 'id', 'string'),
                                        true
                                    ))
                                ))
                                    ->setIsPartOfRenderedBranch(true)
                                    ->addSubNode(
                                        (new IdPropertyPathNode(
                                            'id', new ColumnExpression(
                                                new Table('owningTable', 'id', 'string'),
                                                'id',
                                                'string'
                                            )
                                        ))
                                            ->setIsPartOfRenderedBranch(true)

                                    )
                            )
                    )
            )
        );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friends')->getQuery()->build();
        $collectionQueryBuild2 = $collectionQueryNode->getCollectionQuerySubNode('friends')
            ->getCollectionQuerySubNode('friends')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `friends`.`person1`,`friends`.`person2` FROM `friends` '.
            'JOIN `person` `my_friends` ON `my_friends`.`id` = `friends`.`person2`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `friends`.`person1`,`owningTable`.`id` FROM `friends` '.
            'JOIN `person` `friends_of_my_friends` ON `friends_of_my_friends`.`id` = `friends`.`person2` '.
            'LEFT JOIN `owningTable` ON `inverseTable`.`id` = `owningTable`.`foreignKeyColumnName`',
            $collectionQueryBuild2->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }
}
