<?php

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Table;

class DummyAccessConditionCreation implements AccessControlConditionCreationInterface
{

    function createAccessControlCondition(Table $table): ?ParameterizedSqlInterface
    {
        return null;
    }

    function getRoles(): array
    {
        return [];
    }

    function setRoles(array $roles): AccessControlConditionCreationInterface
    {
        // TODO: Implement setRoles() method.
    }

    function setAccountData(AccountData $accountData): AccessControlConditionCreationInterface
    {
        // TODO: Implement setAccountData() method.
    }
}