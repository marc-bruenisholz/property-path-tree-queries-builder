<?php
/** @noinspection SqlResolve */

/** @noinspection SqlNoDataSourceInspection */

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\PropertyPathTreeModels\IdPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RootPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\ManyToOne;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqlRelationshipModels\OneToOne;
use ch\_4thewin\SqlSelectModels\Page;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

class RenderingOnToManyBranchesTest extends TestCase
{
    public function testPaging()
    {
        $tree = ((new RootPropertyPathNode('person',
            new Table('person', 'id', 'string'), new Page(123, 999))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string','bestFriend'),
                        'bestFriend_id', 'string',
                        new Table('person', 'id', 'string'),
                        new Page(456, 777)
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string', 'bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
            );

        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')->getQuery()->build();
        assertEquals(
            'SELECT * FROM (SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id`,row_number() OVER '.
            '(PARTITION BY `bestFriend`.`bestFriend_id`) as __rowNumber FROM `person` `bestFriend`) `bestFriend` '.
            'WHERE (`bestFriend`.`__rowNumber` > ? AND `bestFriend`.`__rowNumber` < ?)',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person` LIMIT 123 OFFSET 999',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testManyToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string'))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string','bestFriend'),
                        'bestFriend_id','string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string','bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)->setIsNeededForToMany(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'friends',
                            new ManyToMany(
                                new Table('person', 'id', 'string'),
                                new Table('person_person_friends', 'id', 'string'),
                                'person1','string',
                                'person2','string',
                                new Table('person', 'id', 'string', 'friends')
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id', new ColumnExpression(
                                        new Table('person_person_friends', 'id', 'string'),
                                        'person2',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'name', new ColumnExpression(
                                        new Table('person', 'id', 'string', 'friends'),
                                        'name',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getQuery()->build();
        $collectionQueryBuild2 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getCollectionQuerySubNode('friends')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id` FROM `person` `bestFriend`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `person_person_friends`.`person1`,`person_person_friends`.`person2`,`friends`.`name` '.
            'FROM `person_person_friends` JOIN `person` `friends` ON `friends`.`id` = `person_person_friends`.`person2`',
            $collectionQueryBuild2->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testManyToOne()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string'))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string','bestFriend'),
                        'bestFriend_id','string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string','bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'favoriteColor',
                            new ManyToOne(
                                new Table('person', 'id', 'string','bestFriend'),
                                'favoriteColorId','string',
                                new Table('color', 'id', 'string','bestFriend.favoriteColor')
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id', new ColumnExpression(
                                        new Table('person', 'id', 'string','bestFriend'),
                                        'favoriteColorId',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'name', new ColumnExpression(
                                        new Table('color', 'id', 'string','bestFriend.favoriteColor'),
                                        'name',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id`,`bestFriend`.`favoriteColorId`,`bestFriend.favoriteColor`.`name` '.
            'FROM `person` `bestFriend` LEFT JOIN `color` `bestFriend.favoriteColor` ON `bestFriend.favoriteColor`.`id` = `bestFriend`.`favoriteColorId`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testOneToMany()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string'))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)


            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string', 'bestFriend'),
                        'bestFriend_id', 'string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string', 'bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'belongings',
                            new OneToMany(
                                new Table('thing', 'id', 'string', 'bestFriend.belongings'),
                                'ownerId','string',
                                new Table('person', 'id', 'string', 'bestFriend')
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id', new ColumnExpression(
                                        new Table('thing', 'id', 'string'),
                                        'id',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'name', new ColumnExpression(
                                        new Table('thing', 'id', 'string', 'bestFriend.belongings'),
                                        'name',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getQuery()->build();
        $collectionQueryBuild2 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getCollectionQuerySubNode('belongings')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id` FROM `person` `bestFriend`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `bestFriend.belongings`.`ownerId`,`thing`.`id`,`bestFriend.belongings`.`name` FROM `thing` `bestFriend.belongings`',
            $collectionQueryBuild2->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testOwningToInverseOneToOne()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string'))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string', 'bestFriend'),
                        'bestFriend_id','string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string', 'bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'heart',
                            new OneToOne(
                                new Table('person', 'id', 'string', 'bestFriend'),
                                'heartId','string',
                                new Table('heart', 'id', 'string', 'bestFriend.heart'),
                                false
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id', new ColumnExpression(
                                        new Table('person', 'id', 'string', 'bestFriend'),
                                        'heartId',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'heartRate', new ColumnExpression(
                                        new Table('heart', 'id', 'string', 'bestFriend.heart'),
                                        'heartRate',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')
            ->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id`,`bestFriend`.`heartId`,`bestFriend.heart`.`heartRate` '.
            'FROM `person` `bestFriend` LEFT JOIN `heart` `bestFriend.heart` ON `bestFriend.heart`.`id` = `bestFriend`.`heartId`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }

    public function testInverseToOwningOneToOne()
    {
        $tree = ((new RootPropertyPathNode('person', new Table('person', 'id', 'string'))))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id', new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'id',
                        'string'
                    )
                ))->setIsNeededForToMany(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friendsCallingMeBestFriend',
                    (new OneToMany(
                        new Table('person', 'id', 'string', 'bestFriend'),
                        'bestFriend_id', 'string',
                        new Table('person', 'id', 'string')
                    ))
                ))
                    ->setIsPartOfRenderedBranch(true)

                    // Entering ToMany branch here. Anything rendered in this subtree should not be selected
                    // on the base query. No Joins should be made on the base query for columns that are selected
                    // just for rendering in the collection query
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id', new ColumnExpression(
                                new Table('person', 'id', 'string', 'bestFriend'),
                                'id',
                                'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'heart',
                            new OneToOne(
                                new Table('heart', 'id', 'string', 'bestFriend.heart'),
                                'personId', 'string',
                                new Table('person', 'id', 'string', 'bestFriend'),
                                true
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id', new ColumnExpression(
                                        new Table('heart', 'id', 'string', 'bestFriend.heart'),
                                        'id',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                            ->addSubNode(
                                (new LeafPropertyPathNode(
                                    'heartRate', new ColumnExpression(
                                        new Table('heart', 'id', 'string', 'bestFriend.heart'),
                                        'heartRate',
                                        'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)

                            )
                    )
            );
        $builder = new PropertyPathTreeQueriesBuilder(new DummyAccessConditionCreation());
        $collectionQueryNode = $builder->build($tree);
        $collectionQueryBuild1 = $collectionQueryNode->getCollectionQuerySubNode('friendsCallingMeBestFriend')->getQuery()->build();
        $baseQueryBuild = $collectionQueryNode->getQuery()->build();
        assertEquals(
            'SELECT `bestFriend`.`bestFriend_id`,`bestFriend`.`id`,`bestFriend.heart`.`id`,`bestFriend.heart`.`heartRate` '.
            'FROM `person` `bestFriend` LEFT JOIN `heart` `bestFriend.heart` ON `bestFriend`.`id` = `bestFriend.heart`.`personId`',
            $collectionQueryBuild1->getQueryString()
        );
        assertEquals(
            'SELECT `person`.`id` FROM `person`',
            $baseQueryBuild->getQueryString()
        );
    }
}