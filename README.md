# property-path-tree-queries-builder

Builds an SQL query string based on a given property path tree.

This repository is part of the project SQLQueriesByPropertyPaths.