<?php

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Table;

interface AccessControlConditionCreationInterface
{
    function createAccessControlCondition(Table $table): ?ParameterizedSqlInterface;
    function getRoles(): array;
    function setRoles(array $roles): self;
    function setAccountData(AccountData $accountData): self;

}