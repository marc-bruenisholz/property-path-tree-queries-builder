<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\SqlRelationshipModels\AggregateFunction;
use ch\_4thewin\PropertyPathTreeModels\IdPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\ParentPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RootPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\ManyToOne;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqlRelationshipModels\OneToOne;
use ch\_4thewin\SqlRelationshipModels\PropertySortOrder;
use ch\_4thewin\SqlRelationshipModels\SingleCardinalityRelationship;
use ch\_4thewin\SqlRelationshipModels\ToMany;
use ch\_4thewin\SqlRelationshipQueryBuilder\SqlRelationshipQueryBuilder;
use ch\_4thewin\SqlSelectModels\Arguments\IntArgument;
use ch\_4thewin\SqlSelectModels\Ordering;
use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqlSelectModels\SelectedColumn;
use ch\_4thewin\SqlSelectModels\StringInterface;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_AND;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_ANDs;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_GT;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_LT;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;
use ch\_4thewin\TreeTraversal\TreeTraversal;
use ch\_4thewin\TreeTraversal\TreeTraversalInterface;
use RuntimeException;

class PropertyPathTreeQueriesBuilder implements TreeTraversalInterface
{
    // The collection query nodes are used to organize queries as a tree.
    protected CollectionQueryNode $rootCollectionQueryNode;
    protected ?CollectionQueryNode $currentCollectionQueryNode = null;
    protected array $collectionQueryNodeStack = [];

    /**
     * Is used to determine to which sql select
     * a column should be added. Each time a collection (toMany relationship)
     * is encountered a new query is added to the stack. If all sub nodes
     * of that collection node are processed the query is removed
     * from the stack.
     * @var SqlRelationshipQueryBuilder[]
     */
    protected array $queryStack = [];

    protected int $collectionQueryIndex = 0;

    protected ?AccessControlConditionCreationInterface $accessControlConditionCreation = null;

    /**
     * @param AccessControlConditionCreationInterface|null $accessControlConditionCreation
     */
    public function __construct(?AccessControlConditionCreationInterface $accessControlConditionCreation)
    {
        $this->accessControlConditionCreation = $accessControlConditionCreation;
    }


    /**
     * @param RootPropertyPathNode $node
     * @return CollectionQueryNode
     */
    public function build(RootPropertyPathNode $node): CollectionQueryNode
    {
        $this->collectionQueryIndex = 0;

        $baseQuery = new SqlRelationshipQueryBuilder(
            (new Select($node->getFromTable()))->setPage($node->getPage())
        );


        $this->rootCollectionQueryNode = new CollectionQueryNode('root', $baseQuery);
        $this->currentCollectionQueryNode = $this->rootCollectionQueryNode;
        $this->collectionQueryNodeStack[] = $this->rootCollectionQueryNode;

        $idNode = $node->getIdNode();
        $this->handleLeafNode($node, $idNode);

        $baseQuery->getSelect()->setWhereCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));


        // Set WHERE clause using filter condition
        $filterCondition = $node->getFilterCondition();
        if ($filterCondition !== null) {
            // TODO code duplication
            $existingWhereCondition = $baseQuery->getSelect()->getWhereCondition();
            if ($existingWhereCondition !== null) {
                $newWhereCondition = new _AND($existingWhereCondition, $filterCondition);
            } else {
                $newWhereCondition = $filterCondition;
            }
            $baseQuery->getSelect()->setWhereCondition($newWhereCondition);
        }

        (new TreeTraversal($this))->traverse($node);

        // Add search condition to WHERE clause with logical AND
        $searchCondition = $node->getSearchCondition();
        if ($searchCondition) {
            $existingWhereCondition = $baseQuery->getSelect()->getWhereCondition();
//            $newWhereCondition = null;
            if ($existingWhereCondition !== null) {
                $newWhereCondition = new _AND($existingWhereCondition, $node->getSearchCondition());
            } else {
                $newWhereCondition = $node->getSearchCondition();
            }
            $baseQuery->getSelect()->setWhereCondition($newWhereCondition);
        }

        // Root aggregations
        $aggregateFunctions = $node->getAggregateFunctions();
        if(count($aggregateFunctions)>0) {
            $aggregateSelectedColumns = $this->createAggregateSelectedColumns($node, $aggregateFunctions);
            if($node->hasToManyRelationships()) {
                // DISTINCT is added to query. Aggregation function are run before duplicate rows are removed.
                // To aggregate on rows without duplicates, DISTINCT must be used on sub select and aggregation functions
                // in the outer select.
                $innerSelect = clone $baseQuery->getSelect();
                $innerSelect->setPage(null);
                $aggregateQuery = new SqlRelationshipQueryBuilder(new Select($innerSelect));
            } else {
                $aggregateQuery = new SqlRelationshipQueryBuilder(clone $baseQuery->getSelect());
            }
            // Joins need to be added to the base aggregate query because
            // they may filter the resulting rows and may join tables
            // that are used in the where clause
            foreach($baseQuery->getRelationships() as $relationship) {
                $aggregateQuery->addRelationship(clone $relationship);
            }
            $aggregateQuery->getSelect()->setSelectedColumns($aggregateSelectedColumns);
            $aggregateQuery->getSelect()->setPage(null);
            $this->rootCollectionQueryNode->setAggregationQuery($aggregateQuery);
        }

        // Root sort
        $this->addSorting($node->getPropertySortOrders(), $baseQuery);

        return $this->rootCollectionQueryNode;
    }

    /**
     * @param PropertySortOrder[] $propertySortOrders
     * @param SqlRelationshipQueryBuilder $queryBuilder
     * @return void
     */
    protected function addSorting(array $propertySortOrders, SqlRelationshipQueryBuilder $queryBuilder) {
        foreach($propertySortOrders as $propertySortOrder) {
            $queryBuilder->getSelect()->addOrdering(new Ordering($propertySortOrder->getColumnExpression(), $propertySortOrder->getOrder() === 'ASC'));
        }
    }

    /**
     * @param AggregateFunction[] $aggregateFunctions
     * @return SelectedColumn[]
     */
    protected function createAggregateSelectedColumns(ParentPropertyPathNode $node, array $aggregateFunctions): array
    {
        $aggregateSelectedColumns = [];
        foreach ($aggregateFunctions as $aggregateFunction) {
            $propertyName = $aggregateFunction->getPropertyName();
            $functionName = $aggregateFunction->getFunctionName();
            /** @var LeafPropertyPathNode $aggregateSubNode */
            $aggregateSubNode = $node->getSubNode($propertyName);
            // TODO type safe function to get leaf node
            // TODO validation layer has to make sure that property to aggregate exists as a leaf
            $prototypeColumnExpression = $aggregateSubNode->getColumnExpression();
            $aggregateColumnExpression = new ColumnExpression(
                $prototypeColumnExpression->getTable(),
                $prototypeColumnExpression->getColumnName(),
                'integer', // TODO only support integer aggregations?
                // TODO assuming function name is already validated and safe
                $functionName . '(${columnReference})'
            );
            $aggregateSelectedColumn = new SelectedColumn($aggregateColumnExpression);
            $aggregateSelectedColumns[] = $aggregateSelectedColumn;
        }
        return $aggregateSelectedColumns;
    }

    /**
     * Creates the column expression for the foreign key column that is later used to match the
     * rows with the primary key of the other side of the ToMany relationship.
     * @param ToMany $relationship
     * @return ColumnExpression
     */
    protected function getForeignKeyColumnForToManyRelationship(
        ToMany $relationship
    ): ColumnExpression
    {
        $columnName = null;
        $columnType = null;
        $table = null;
        if ($relationship instanceof OneToMany) {
            $table = $relationship->getTargetTable();
            $columnName = $relationship->getForeignKeyColumnName();
            $columnType = $relationship->getForeignKeyColumnType();
        } elseif ($relationship instanceof ManyToMany) {
            $table = $relationship->getOneToMany()->getTargetTable();
            $columnName = $relationship->getOneToMany()->getForeignKeyColumnName();
            $columnType = $relationship->getOneToMany()->getForeignKeyColumnType();
        }
        return new ColumnExpression($table, $columnName, $columnType);
    }

    protected function handleLeafNode(ParentPropertyPathNode $parentPropertyPathNode, ?LeafPropertyPathNode $node): void
    {
        if ($node !== null) {
//            $isSelected = false;
            if ($node instanceof IdPropertyPathNode) {
                $isSelected = $node->isPartOfRenderedBranch() || (
                        $parentPropertyPathNode->isPartOfRenderedBranch() && (
                            $node->isNeededForToMany() || $node->isNeededForToOne()
                        )
                    );
            } else {
                $isSelected = $node->isPartOfRenderedBranch();
            }
            if ($isSelected) {
                if (count($this->queryStack) === 0) {
                    // An ID column is only selected on the base query if there
                    // is no ToMany branch active (queryStack count is 0).
                    $baseQuery = $this->rootCollectionQueryNode->getQuery();
                    $baseQuery->getSelect()->selectColumn($node->getColumnExpression());
                } else {
                    // topQuery has to be reset because a new query might have been created.
                    $topQuery = $this->queryStack[count($this->queryStack) - 1];
                    $topQuery->getSelect()->selectColumn($node->getColumnExpression());
                }
            }
        }
    }

    /**
     * @param NodeInterface $node
     * @param NodeInterface|null $parentNode
     * @param array $branch
     * @return bool
     */
    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        /** @var ParentPropertyPathNode $parentNode */

        $baseQuery = $this->rootCollectionQueryNode->getQuery();

        $queryStackCount = count($this->queryStack);
        $topQuery = null;
        if ($queryStackCount > 0) {
            $topQuery = $this->queryStack[$queryStackCount - 1];
        }

        if ($node instanceof RelationshipPropertyPathNode) {
            $relationship = $node->getRelationship();

            // For every join, the access control condition has to be added as well

            // BASE QUERY Add only relationships to SQL builder where SQL JOINs are needed
            if ($relationship instanceof ManyToMany) {
                $relationship->getManyToOne()->setAccessControlCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));
                if ($node->isPartOfFilteredBranch()) {
                    $relationship->getOneToMany()->setJoinType('INNER');
                    if ($node->isOnlyIdRestricted()) {
                        // TODO isOnlyIdRestricted flag is not set currently. Possible optimization for later.
                        // If there are no other columns part of the search other than the ID column,
                        // only the intermediary table has to be joined
                        $baseQuery->addRelationship($relationship->getOneToMany());
                    } else {
                        // If there are columns other than the ID column part of the search,
                        // the intermediary and the third table (right side of the relationship) have to be joined.
                        $baseQuery->addRelationship($relationship);
                    }
                } elseif($node->isPartOfSearchedBranch()) {
                    $baseQuery->addRelationship($relationship);
                }
            } elseif ($relationship instanceof ManyToOne) {
                $relationship->setAccessControlCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));

                // $topQuery === null means that this relationship appears directly on the root node. So
                // it is added to the base query. Otherwise the relationship is added to the corresponding collection query.
                if (($node->isPartOfFilteredBranch() && !$node->isOnlyIdRestricted()) ||
                    ($topQuery === null && ($node->isPartOfRenderedBranch() && !$node->isOnlyIdRendered()))) {
                    if ($node->isPartOfFilteredBranch()) {
                        // Even if this relationship does not have a filter definition,
                        // if it is part of a filtered branch, only JOIN type INNER makes sense for all
                        // JOINs leading up to the one with a condition.
                        $relationship->setJoinType('INNER');
                    }
                    // Join is only needed for columns other than the ID column.
                    // The foreign key column on the left table contains the ID of the right table.
                    $baseQuery->addRelationship($relationship);
                } elseif($node->isPartOfSearchedBranch()) {
                    // searching and filtering is always done on the base query. so no $topQuery === null check necessary.
                    // TODO possible optimization if search only uses ID in foreign key column. see above
                    //  isOnlyIdRestricted is currently not set
                    $baseQuery->addRelationship($relationship);
                }
            } elseif ($relationship instanceof OneToMany) {
                $relationship->setAccessControlCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));

                if ($node->isPartOfFilteredBranch()) {
                    $relationship->setJoinType('INNER');

                    // Joins for ToMany relationships are only relevant on the base query if
                    // columns are searched.
                    $baseQuery->addRelationship($relationship);
                } elseif($node->isPartOfSearchedBranch()) {
                    $baseQuery->addRelationship($relationship);
                }
            } elseif ($relationship instanceof OneToOne) {
                $relationship->setAccessControlCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));

                if ($relationship->isFromInverseToOwningTable()) {
                    // For OneToOne relationships with the foreign key column on the right table,
                    // it has to be joined in any case because the ID column is on the right table.
                    // The ID column is needed to determine whether the property should be set to NULL if
                    // there is no object on the right side of the relationship.
                    if ($node->isPartOfFilteredBranch() ||
                        ($topQuery === null && ($node->isPartOfRenderedBranch() || $node->getIdNode()->isNeededForToOne()))) {
                        if ($node->isPartOfFilteredBranch()) {
                            $relationship->setJoinType('INNER');
                        }

                        $baseQuery->addRelationship($relationship);
                    }  elseif($node->isPartOfSearchedBranch()) {
                        $baseQuery->addRelationship($relationship);
                    }
                } else {
                    if (($node->isPartOfFilteredBranch() && !$node->isOnlyIdRestricted()) ||
                        ($topQuery === null && ($node->isPartOfRenderedBranch() && !$node->isOnlyIdRendered()))) {
                        if ($node->isPartOfFilteredBranch()) {
                            $relationship->setJoinType('INNER');
                        }
                        // Same as for OneToMany.
                        // For owning-to-inverse OneToOne,
                        // a table join is only needed for columns other than the ID column.
                        // The foreign key column on the left table contains the ID of the right table.
                        $baseQuery->addRelationship($relationship);
                    } elseif($node->isPartOfSearchedBranch()) {
                        $baseQuery->addRelationship($relationship);
                    }
                }
            }

            // BASE QUERY set distinct when ToMany fields are filtered or searched
            if ($relationship instanceof ToMany && ($node->isPartOfFilteredBranch() || $node->isPartOfSearchedBranch())) {
                // If a field in a ToMany branch is restricted,
                // rows with the same id might appear several times in the result
                // of the base query so that the field on the right side of
                // the ToMany relationship can be filtered. After filtering,
                // DISTINCT removes any duplicate rows. This works
                // because fields in the ToMany branch never appear
                // as a selected column but only in JOIN or WHERE conditions.
                $baseQuery->getSelect()->setIsDistinct(true);
            }


            // COLLECTION QUERY Add only relationships to SQL builder where SQL JOINs are needed
            if ($topQuery !== null) {
                // For collection queries, searched/filtered columns do not matter, only rendered columns.
                // ToMany relationships do not matter as well because new queries are created for them instead of
                // just joins. For the base query, ToMany matters if it was part of a search/filter.
                if ($relationship instanceof ManyToOne && ($node->isPartOfRenderedBranch() && !$node->isOnlyIdRendered())) {
                    // Join is only needed for ManyToOne if there are columns rendered other than the ID column
                    $topQuery->addRelationship($relationship);
                } elseif ($relationship instanceof OneToOne) {
                    if ($relationship->isFromInverseToOwningTable()) {
                        if ($node->isPartOfRenderedBranch() ||
                            ($node->getIdNode() !== null && $node->getIdNode()->isNeededForToOne())) {
                            // Add join only for inverse-to-owning OneToOne if anything is rendered
                            // or if the id is needed (which should always be the case I think)
                            $topQuery->addRelationship($relationship);
                        }
                    } else {
                        // For owning-to-inverse OneToOne, only non-ID columns that should
                        // be rendered are relevant here for a join
                        if ($node->isPartOfRenderedBranch() && !$node->isOnlyIdRendered()) {
                            $topQuery->addRelationship($relationship);
                        }
                    }
                }
            }

            // COLLECTION QUERY CREATION
            // Only if something is rendered, a collection query is needed.
            if ($relationship instanceof ToMany && $node->isPartOfRenderedBranch()) {
//                $collectionQuery = null;
                $foreignKeyColumnExpression = $this->getForeignKeyColumnForToManyRelationship($relationship);
//                $foreignKeyColumnType = null;
                if ($relationship instanceof ManyToMany) {
                    $foreignKeyColumnType = $relationship->getOneToMany()->getForeignKeyColumnType();
                    // create collection query for ManyToMany only if it is needed.
                    // That is, if there are columns rendered
                    $rootTableForCollectionQuery = $relationship->getOneToMany()->getOwningTable();
                    // Collection queries are not affected by restrictions.
                    $collectionQuery = new SqlRelationshipQueryBuilder(new Select($rootTableForCollectionQuery));
                    if (!$node->isOnlyIdRendered()) {
                        // If only the ID column is rendered, the intermediary table is
                        // enough. If there are more columns rendered, the table on the right side
                        // of the relationship has to be joined here.
                        $relationship->getManyToOne()->setJoinType('INNER');
                        $collectionQuery->addRelationship($relationship->getManyToOne());
                    }
                } elseif ($relationship instanceof OneToMany) {
                    $foreignKeyColumnType = $relationship->getForeignKeyColumnType();
                    $rootTableForCollectionQuery = $relationship->getOwningTable();
                    $collectionQuery = new SqlRelationshipQueryBuilder(new Select($rootTableForCollectionQuery));
                } else {
                    // @codeCoverageIgnoreStart
                    throw new RuntimeException(
                        'implementation error. No branch for ' . get_class($relationship) . ' implemented.'
                    );
                    // @codeCoverageIgnoreEnd
                }
                // Collection queries are not affected by search conditions.
                $collectionQuery->setIsIgnoringRelationshipSearchConditions(true);

                // The foreign key is needed to match result rows to an object owning the collection.
                $collectionQuery->getSelect()->selectColumn($foreignKeyColumnExpression);

                // Access control on collection queries
                $collectionQuery->getSelect()->setWhereCondition($this->accessControlConditionCreation->createAccessControlCondition($node->getTargetTable()));

                // Limit output for this collection query
                // TODO Might be confusing. query is later transformed and page is set to null. There is no OFFSET/LIMIT
                //  on collection queries (other than the root/base query). See transformCollectionQueryForPagination()
                $collectionQuery->getSelect()->setPage($relationship->getPage());

                // Aggregations
                $aggregateQuery = $this->createAggregationQueryFromCollectionQuery($node, $collectionQuery->getSelect());

                // Sorting on collection
                $this->addSorting($relationship->getPropertySortOrders(), $collectionQuery);

                $node->setSqlQueryIndex($this->collectionQueryIndex++);
                // Add collection query to stack so that columns or joins of sub nodes can be added to it.
                $this->queryStack[] = $collectionQuery;

                // Create new node for collection query and add it to the parent node
                $newCollectionQueryNode = new CollectionQueryNode($node->getName(), $collectionQuery, $foreignKeyColumnType, $aggregateQuery);
                $this->currentCollectionQueryNode->addSubNode($newCollectionQueryNode);
                $this->currentCollectionQueryNode = $newCollectionQueryNode;
                $this->collectionQueryNodeStack[] = $newCollectionQueryNode;
            }

            // FILTER CONDITION PLACEMENT
            $filterCondition = $node->getFilterCondition();
            if ($filterCondition !== null) {
                if ($relationship instanceof ManyToMany) {
                    // TODO set isOnlyIdRestricted
                    if ($node->isOnlyIdRestricted()) {
                        // If there are no other columns part of the search other than the ID column,
                        // the search condition is placed in the JOIN condition that joins the intermediary table.
                        $relationship->getOneToMany()->setSearchCondition($filterCondition);
                    } else {
                        // If there are columns other than the ID column part of the search,
                        // the search condition is placed in the JOIN condition that joins the table to the right of the
                        // intermediary table.
                        $relationship->getManyToOne()->setSearchCondition($filterCondition);
                    }
                } elseif ($relationship instanceof ManyToOne || ($relationship instanceof OneToOne && !$relationship->isFromInverseToOwningTable())) {
                    if ($node->isOnlyIdRestricted()) {
                        // TODO whole optimization branch is currently never visited as isOnlyIdRestricted is not set.
                        //  Implement optimization after software becomes more stable.
                        // only ID is searched. The ID exists on the left table of the relationship
                        // the search condition of the left table is extended with the ID search condition using a logical AND.
                        // If the left table is the root table, set the search condition in the where clause,
                        // set it in the JOIN condition otherwise.
                        if ($parentNode instanceof RootPropertyPathNode) {
                            $currentSearchCondition = $baseQuery->getSelect()->getWhereCondition();
                            $newSearchCondition = $currentSearchCondition === null ? $filterCondition : new _AND(
                                $currentSearchCondition, $filterCondition
                            );
                            $baseQuery->getSelect()->setWhereCondition($newSearchCondition);
                        } elseif ($parentNode instanceof RelationshipPropertyPathNode) {
                            // If parent node depicts a SingleCardinalityRelationship, the "left"
                            // table is just the table that is joined for that relationship.
                            // For ManyToMany, the table right to the intermediary table becomes the "left" table.
                            $parentRelationship = $parentNode->getRelationship();

                            // TODO too much complexity (branches) for simple logic
                            $currentSearchCondition = null;
                            if ($parentRelationship instanceof SingleCardinalityRelationship) {
                                $currentSearchCondition = $parentRelationship->getSearchCondition();
                            } elseif ($parentRelationship instanceof ManyToMany) {
                                $currentSearchCondition = $parentRelationship->getManyToOne()->getSearchCondition();
                            }

                            $newSearchCondition = $currentSearchCondition === null ? $filterCondition : new _AND(
                                $currentSearchCondition, $filterCondition
                            );

                            if ($parentRelationship instanceof SingleCardinalityRelationship) {
                                $parentRelationship->setSearchCondition($newSearchCondition);
                            } elseif ($parentRelationship instanceof ManyToMany) {
                                $parentRelationship->getManyToOne()->setSearchCondition($newSearchCondition);
                            }
                        } else {
                            // @codeCoverageIgnoreStart
                            throw new RuntimeException(
                                'implementation error. No branch for ' . get_class($parentNode) . ' implemented.'
                            );
                            // @codeCoverageIgnoreEnd
                        }
                    } else {
                        // search condition is placed on JOIN condition that joins the right table
                        $relationship->setSearchCondition($filterCondition);
                    }
                } elseif ($relationship instanceof OneToMany || ($relationship instanceof OneToOne && $relationship->isFromInverseToOwningTable())) {
                    // Search condition is always added to the right table in this case.
                    // In other words, no optimizations possible in case only the ID is searched.
                    $relationship->setSearchCondition($filterCondition);
                }
            }


            // Select ID column for base query or collection query
            // ID node is not part of array returned by getSubNodes(). Thus, it is not traversed and must
            // be handled separately.
            assert($parentNode instanceof ParentPropertyPathNode);
            $this->handleLeafNode($parentNode, $node->getIdNode());
        } elseif ($node instanceof LeafPropertyPathNode) {
            // A IdPropertyPathNode is never traversed. IdPropertyPathNodes are handled when the root node
            // or RelationshipPropertyPathNodes are handled.
            // A leaf node generates a selected sql column. Columns are added to the base query
            // if it is not part of a ToMany and rendered or if it is searched.
            // If it is part of a ToMany branch, the query stack count is greater than 0 because
            // a collection query is added for every relevant ToMany relationship.
            // Only the base query is used for searches so every searched column is also added to the base query.
            assert($parentNode instanceof ParentPropertyPathNode);
            $this->handleLeafNode($parentNode, $node);
        }

        return true;
    }

    /**
     * @param NodeInterface $node
     * @param NodeInterface|null $parentNode
     * @param array $branch
     * @return void
     */
    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
        if ($node instanceof RelationshipPropertyPathNode) {
            $relationship = $node->getRelationship();
            if ($relationship instanceof ToMany && $node->isPartOfRenderedBranch()) {
                // As all children have been processed of this collection (toMany) node,
                // the collection query must be popped from the stack.
                array_pop($this->queryStack);

                /** @var CollectionQueryNode $finishedCollectionQueryNode */
                $finishedCollectionQueryNode = array_pop($this->collectionQueryNodeStack);
                $this->currentCollectionQueryNode = $this->collectionQueryNodeStack[count($this->collectionQueryNodeStack) - 1];

                $this->transformCollectionQueryForPagination($finishedCollectionQueryNode);

            }
        }
    }

    protected function createAggregationQueryFromCollectionQuery(RelationshipPropertyPathNode $node, Select $collectionQuerySelect): ?SqlRelationshipQueryBuilder
    {
        $aggregateSelect = clone $collectionQuerySelect;
        $relationship = $node->getRelationship();
        if ($relationship instanceof ToMany) {
            $aggregateFunctions = $relationship->getAggregateFunctions();
            if (count($aggregateFunctions) === 0) {
                return null;
            }
            $aggregateSelectedColumns = $this->createAggregateSelectedColumns($node, $aggregateFunctions);
            $selectedColumns = [];
            // In collection query for ToMany relationship, first column is always FK to the object holding the ToMany collection.
            // TODO allow composite primary keys
            $firstColumn = $collectionQuerySelect->getSelectedColumns()[0];
            $selectedColumns[] = $firstColumn;
            foreach ($aggregateSelectedColumns as $aggregateSelectedColumn) {
                $selectedColumns[] = $aggregateSelectedColumn;
            }
            $aggregateSelect->setSelectedColumns($selectedColumns);
            $aggregateSelect->setPage(null);
            $aggregateSelect->setGroupByColumnExpressions([$firstColumn->getColumnExpression()]);
        } else {
            throw new RuntimeException('implementation error');
        }
        return new SqlRelationshipQueryBuilder($aggregateSelect);
    }

    protected function transformCollectionQueryForPagination(CollectionQueryNode $finishedCollectionQueryNode)
    {
        // Transform LIMIT/OFFSET pagination
        // Problem: LIMIT/OFFSET is applied to the whole result set. But in a collection query, the result set
        // contains rows that may be mapped to different parent objects. The pagination should be the
        // same as if there was a separate query for each parent object. To avoid the 1+n problem, only
        // one collection query is run to get all collection elements of different parent objects.
        // If a counter per parent ID is used that is incremented any time a corresponding row appears,
        // the counter can be used to apply pagination per collection.
        // The window function ROW_NUMBER() can generate that counter. If the window function is partitioned
        // by the parent id, the counter increments independently per parent id. The pagination can be applied
        // in the WHERE clause. The sub select is needed because the ROW_NUMBER() result cannot be used in the
        // WHERE clause of the same SELECT where it is used.
        $baseSelect = $finishedCollectionQueryNode->getQuery()->getSelect();
        $page = $baseSelect->getPage();
        if ($page !== null) {
            $baseSelectColumns = $baseSelect->getSelectedColumns();
            // First column must be the foreign key column connecting to the left side of the relationship.
            // Use it to add a column containing the row number for the given parent id in the foreign key column.
            $baseSelect->selectColumn(new class($baseSelectColumns[0]->getColumnExpression()) implements StringInterface {
                protected ColumnExpression $foreignKeyColumnExpression;

                public function __construct(ColumnExpression $foreignKeyColumnExpression)
                {
                    $this->foreignKeyColumnExpression = $foreignKeyColumnExpression;
                }

                function toString(): string
                {
                    return 'row_number() OVER (PARTITION BY ' . $this->foreignKeyColumnExpression->toString() . ') as __rowNumber';
                }
            });
            $innerFromTable = $baseSelect->getFromClause();
            if (!($innerFromTable instanceof Table)) {
                throw new RuntimeException('implementation error. FROM clause in base select must be table');
            }
            $paginationSelect = new Select($baseSelect);

            // Create where condition to apply pagination on rowNumber. rowNumber starts at 1. Offset starts at 0.
            $paginationWhereCondition = new _ANDs();
            $paginationWhereCondition->addCondition(new _GT(
                new ColumnExpression($innerFromTable, '__rowNumber', 'integer'),
                new IntArgument($baseSelect->getPage()->getOffset())));
            $paginationWhereCondition->addCondition(new _LT(
                new ColumnExpression($innerFromTable, '__rowNumber', 'integer'),
                new IntArgument($baseSelect->getPage()->getOffset() + $baseSelect->getPage()->getLimit() + 1)
            ));
            $paginationSelect
                // empty array leads to wildcard "SELECT * FROM ..."
                ->setSelectedColumns([])
                ->setWhereCondition($paginationWhereCondition);
            $finishedCollectionQueryNode->getQuery()->setSelect($paginationSelect);
            $baseSelect->setPage(null);
        }
    }
}