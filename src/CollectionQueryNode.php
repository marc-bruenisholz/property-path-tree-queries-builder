<?php

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

use ch\_4thewin\SqlRelationshipQueryBuilder\SqlRelationshipQueryBuilder;
use ch\_4thewin\SqppTreeTraversalModels\Node;
use TypeError;

class CollectionQueryNode extends Node
{
    /**
     * @var SqlRelationshipQueryBuilder The query builder associated with this node
     */
    protected SqlRelationshipQueryBuilder $query;

    protected ?string $foreignKeyColumnType;

    protected ?SqlRelationshipQueryBuilder $aggregationQuery;

    /**
     * @param string $name
     * @param SqlRelationshipQueryBuilder $query
     * @param string|null $foreignKeyColumnType
     * @param SqlRelationshipQueryBuilder|null $aggregationQuery
     */
    public function __construct(string $name, SqlRelationshipQueryBuilder $query, ?string $foreignKeyColumnType = null, ?SqlRelationshipQueryBuilder $aggregationQuery = null)
    {
        parent::__construct($name);
        $this->query = $query;
        $this->foreignKeyColumnType = $foreignKeyColumnType;
        $this->aggregationQuery = $aggregationQuery;
    }


    public function getCollectionQuerySubNode(string $name): ?CollectionQueryNode {
        // The addSubNode method only allows CollectionQueryNode nodes.
        // Hence, type is guaranteed to be CollectionQueryNode
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getSubNode($name);
    }

    /**
     * @return CollectionQueryNode[]
     */
    public function getCollectionQuerySubNodes(): array {
        return parent::getSubNodes();
    }

    /**
     * @param CollectionQueryNode $node
     * @return Node
     */
    public function addSubNode($node): Node
    {
        // Removed interface-incompatible type hint NodeInterface before function parameter $node and added runtime type check instead.
        if(!($node instanceof CollectionQueryNode)) {
            throw new TypeError();
        }
        return parent::addSubNode($node);
    }

    /**
     * @return SqlRelationshipQueryBuilder
     */
    public function getQuery(): SqlRelationshipQueryBuilder
    {
        return $this->query;
    }

    /**
     * @return string|null
     */
    public function getForeignKeyColumnType(): ?string
    {
        return $this->foreignKeyColumnType;
    }

    /**
     * @return SqlRelationshipQueryBuilder|null
     */
    public function getAggregationQuery(): ?SqlRelationshipQueryBuilder
    {
        return $this->aggregationQuery;
    }

    /**
     * @param SqlRelationshipQueryBuilder|null $aggregationQuery
     */
    public function setAggregationQuery(?SqlRelationshipQueryBuilder $aggregationQuery): void
    {
        $this->aggregationQuery = $aggregationQuery;
    }

   


}