<?php

namespace ch\_4thewin\PropertyPathTreeQueriesBuilder;

class AccountData
{
    /** @var array */
    protected $roles;
    
    /** @var string */
    protected $userId;

    /**
     * @param array $roles
     * @param string $userId
     */
    public function __construct(array $roles, string $userId)
    {
        $this->roles = $roles;
        $this->userId = $userId;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }
    
    
}